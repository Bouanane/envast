﻿using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject loadingScreen;

    private void Start()
    {
        PlayerPrefs.DeleteAll();   
    }

    public void play()
    {
        loadingScreen.SetActive(true);
    }

}
