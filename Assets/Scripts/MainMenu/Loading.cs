﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    public int sceneToLoad;
    AsyncOperation loadingOperation;

    public Slider progressBar;
    [SerializeField] Text m_Text;

    void Start()
    {
        StartCoroutine(LoadScene(sceneToLoad));
    }

    protected virtual IEnumerator LoadScene(int scene)
    {
        yield return null;

        loadingOperation = SceneManager.LoadSceneAsync(sceneToLoad);
        loadingOperation.allowSceneActivation = false;
        Debug.Log("Pro :" + loadingOperation.progress);
        while (!loadingOperation.isDone)
        {
            m_Text.text = "Loading progress: " + (loadingOperation.progress * 100) + "%";
            progressBar.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);

            if (loadingOperation.progress >= 0.9f)
            {
                m_Text.text = "Touch Anywhere to Play";
                if (Input.GetMouseButtonDown(0))
                    loadingOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}
