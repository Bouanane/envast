﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class Timer : MonoBehaviour
{
    public float timeLeft = 60f;
    int intTime;
    public Text startText;

    public GameObject LostUI;
    public Animator anim;


    void Update()
    {
        timeLeft -= 1* Time.deltaTime;
        intTime = Mathf.RoundToInt(timeLeft);
        startText.text = (intTime).ToString("0");
        if (intTime < 0)
        {
            GameManager.Instance.hideUI();
            startText.text = "0";
            LostUI.SetActive(true);
            StartCoroutine(pause());
            anim.SetBool("Trigger", true);
        }
    }
    
    IEnumerator pause()
    {
        yield return new WaitForSeconds(1.5f);
        Time.timeScale = 0; 
    }
}
