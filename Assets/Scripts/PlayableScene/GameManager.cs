﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    static public GameManager Instance;
    public Coins coins;

    int currentScene;

    int ArrowCount = 3;
    int onCount = 0;
    [SerializeField] int failedAttempts = 0;
    [SerializeField] int nextSceneIDX;

    [SerializeField] GameObject win;
    [SerializeField] GameObject RepeatBtn;
    
    [SerializeField] GameObject[] stars;
    
    [SerializeField] Animator anim;
    [SerializeField] Animator changeScene;

    [SerializeField] GameObject[] hidden;
    [SerializeField] GameObject pnel;

    AudioSource src;

    [SerializeField] AudioClip clip;


    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        //Managing Audio
        src = GetComponent<AudioSource>();
        src.PlayOneShot(clip);
        src.loop = true;

        currentScene = SceneManager.GetActiveScene().buildIndex;
    }

    public void ArrowsCounnected(int arrow)
    {
        onCount = onCount + 1;
        if (onCount == ArrowCount)
        {
            hideUI();

            win.SetActive(true);
            rate();
            anim.SetBool("Trigger", true);
        }
    }

    public void Trials(int fails)
    {
        failedAttempts = failedAttempts + 1;
    }

    public void rate()
    {
        if (failedAttempts == 0)
        {
            coins.addScore(100);
            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].SetActive(true);
            }
        }

        if (failedAttempts >= 1 && failedAttempts < 4)
        {
            coins.addScore(60);
            stars[0].SetActive(true);
            stars[1].SetActive(true);
        }

        if (failedAttempts > 3 && failedAttempts < 5)
        {
            coins.addScore(30);
            stars[0].SetActive(true);
        }
        else if (failedAttempts > 6)
        {
            coins.addScore(10);
        }
    }

    public void hideUI()
    {
        win.SetActive(true);
        pnel.SetActive(true);
        for (int i = 0; i < hidden.Length; i++)
        {
            hidden[i].SetActive(false);
        }

    }

    //Lost Case
    public void nextScene()
    {
        PlayerPrefs.SetInt("score", coins.score);
        StartCoroutine(changeScen(nextSceneIDX));
        changeScene.SetBool("isOPen", true);
    }
    public void Retry()
    {
        SceneManager.LoadScene(currentScene);
    }

    public void backMenu()
    {
        SceneManager.LoadScene(0);
    }

    IEnumerator changeScen(int index)
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(index);
    }
}
