﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    Image progressBar;
    public float maxTrial = 3f;
    float timeLeft;

    void Start()
    {
        progressBar = GetComponent<Image>();
        timeLeft = maxTrial;
    }

    void Update()
    {
        if (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            progressBar.fillAmount = timeLeft / maxTrial;
        }
        else
        {
            Time.timeScale = 0;
        }
    }
}