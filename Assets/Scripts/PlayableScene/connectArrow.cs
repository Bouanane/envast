﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class connectArrow : MonoBehaviour
{
    static public connectArrow ArrowsInstance;
    public Image progressBar;

    Vector3 startPoint;
    public Vector3 startPosition;
    public SpriteRenderer tail;



    void Start()
    {
        
        startPoint = transform.parent.position;
        startPosition = transform.position;
    }

    private void OnMouseDrag()
    {
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        newPosition.z = 0;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(newPosition, 0.2f);
        foreach (Collider2D collider in colliders)
        {
            if (collider.gameObject != gameObject)
            {
                updateArrow(collider.transform.position);
                
               
                if (transform.parent.tag.Equals(collider.transform.parent.tag))
                {
                    Done();
                }
               
                return;
            }
            
        }

        updateArrow(newPosition);
         
    }

    void Done()
    {
        GameManager.Instance.ArrowsCounnected(1);
        progressBar.fillAmount += 0.33f;
        Destroy(this);
    }

    private void OnMouseUp()
    {
        updateArrow(startPosition);
        GameManager.Instance.Trials(1);
    }

    void updateArrow(Vector3 newPosition)
    {
        //update the arrow position
        transform.position = newPosition;

        //update direction
        Vector3 direction = newPosition - startPoint;
        transform.right = direction * transform.lossyScale.x;

        float dist = Vector2.Distance(startPoint, newPosition);
        tail.size = new Vector2(dist, tail.size.y);
    }

    public void reset()
    {
        updateArrow(startPosition);
    }
}
