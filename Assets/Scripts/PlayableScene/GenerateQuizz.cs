﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateQuizz : MonoBehaviour
{
    static public GenerateQuizz QuizzInstance;
    [SerializeField]
    Image[] quizzPicture;

    public Sprite[] options;
    public Text[] animalName;

    public GameObject[] leftDots;
    public GameObject[] rightDots;

    //Generate the Quizz on the First Frame
    void Start()
    {
        Genrate();
    }

    //Methode I am using to Generate the Quizz Randomly
    public void Genrate()
    {
        //Randomise Pictures without deplications
        Random.InitState(Random.Range(1,100));
        for (int i = 0; i < quizzPicture.Length; i++)
        {
            quizzPicture[i].sprite = options[Random.Range(0,options.Length)];
            leftDots[i].name = quizzPicture[i].sprite.name.ToString();

            animalName[i].text = quizzPicture[i].sprite.name.ToString();
            rightDots[i].name = quizzPicture[i].sprite.name.ToString();
        }
    }

}
