﻿using UnityEngine;
using UnityEngine.UI;

public class Coins : MonoBehaviour
{
    public Text coinsText;
    public int score;

    private void Start()
    {
        score = PlayerPrefs.GetInt("score");
        coinsText.text = score.ToString();
    }
    public void addScore(int amount)
    {
        score = score + amount;
        coinsText.text = score.ToString();
    }
}
