Greeting, this is the fruit of almost 15 hours of working.

[[_TOC_]]

## Intro

As Mentioned before it took about 15 hours to finish the game up, I have got to few problems I never encountred before, the hardest was that I have no clue how to connect GameObjects with a Line Renderer manually, so I took another approche which is a physic based solution which worked pretty well, the project can be hardly described as finished since it is still lack soundtracks and Animation can be tuned, my focus was mainly in the game logic since I am honestly not too familiar to making puzzle and quiz Games.  

## What Did I use to make the application

1. Unity
    - I tried to keep things simple so I did not use any external or Third-Party Packages.
    - Almost the whole project was made using Unity UI.


2. Figma
    - All the sprites and UI elements were drawn using Figma.

## Time Devision

I originally intended the game to be completed in 8 - 12hours but it took a bit more than that.

1. Planing 
    - Like I said before I am not too familiar with quiz making, I took my time to write down and google some ideas, also I tried to think about each section of the test alone then connect the dots.

2. Making a prototype
    - It is a habit of mine to prototype my games first, I wrote few scripts and made a simple user interface just to test my logic which was a bit of at the start, yet helped me better understand the task at hand.

3. User Interface/User Experience
    - Like any good game UI is important so I took the "Simple yet Appealing" approche since I did not want to spend too much time getting of the point.
    - As for the animations I tried few simple ones which I am sure I can make them better giving the time.

4. Hunting the Bugs
    - After aplying the new UI, it is time to hit the scripts again, which took a bit more time than expected since I kept changing the logic for a while.

## Problems I faced
Actually it was just one major problem , Line Renderering , since I never really felt like using them before, they were a bit hard to get into, sadly I did not have enough time to dig much into the topic, since I have few other priorities (College, Workshops, and my freelancing jobs).

## Conclusion
The game is now working as intended more or less although I can't hide the fact that it needs a bit more work to make it better.

## Outro
I really want to thank you for giving me the opportunity to learn couple new things during the hours I spent working, It was fun and bit of a bizzare challange yet it had its perks.